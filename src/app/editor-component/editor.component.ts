import { AfterViewInit, Component, ViewChild, ViewContainerRef } from "@angular/core";
import { ICellEditorAngularComp } from "@ag-grid-community/angular";
import { ICellEditorParams } from 'ag-grid-community';

@Component({
   selector: 'app-editor-component',
   template: `
      <input class="validatedInput" [(ngModel)]="value" #input (keydown)="onKeyDown($event)" />
    `,
    styles: [`
      .eventProxy {
        display: none;
      }
      .validatedInput {
        outline: none;
        width: 100%;
        height: 100%;
        border: none;
        padding: 0px 8px;
      }
    `]
})
export class EditorComponent implements ICellEditorAngularComp, AfterViewInit {
   private params!: ICellEditorParams; 
   public value!: string;

   // @ts-ignore
   @ViewChild('input', { read: ViewContainerRef }) public input: ViewContainerRef;

   ngAfterViewInit() {
       // focus on the input
       setTimeout(() => this.input.element.nativeElement.focus());
   }

   // @ts-ignore
   agInit(params: ICellEditorParams): void {
       this.params = params;

       this.value = this.params.value
   }

   /* Component Editor Lifecycle methods */
   // the final value to send to the grid, on completion of editing
   getValue() {
       // this simple editor doubles any value entered into the input
       return 'hello';
   }

   // Gets called once before editing starts, to give editor a chance to
   // cancel the editing before it even starts.
   isCancelBeforeStart() {
       return false;
   }

   // Gets called once when editing is finished (eg if Enter is pressed).
   // If you return true, then the result of the edit will be ignored.
   isCancelAfterEnd() {
       // our editor will reject any value greater than 1000
       return !!this.value;
   }

   public onKeyDown(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      const isOk = this.validate();
      if (!isOk) {
        event.preventDefault();
      }
    }
  }

  public validate() {
    return true;
  }
}
